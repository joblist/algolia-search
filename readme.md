Available on [npm:joblist-algolia-search](https://www.npmjs.com/package/joblist-algolia-search)

Check out `/index.html` for usage.

# Usage

insert the `<algolia-search/>` custom web element with the
correct configuration attributes for your application.

```html
<algolia-search
	app-id=""
	api-key=""
	index-names=""
	routing=""
	autofocus=""
	show-reset=""

	pub-id=""
	origin=""
></algolia-search>
```

If you want search results on a map (or just a map with markers):

```html
<map-list longitude="" latitude="" zoom="" markers="" origin=""></map-list>
```

After importing the script from a cdn, either in a javascript file or in a html file (see the file `./index.html` for full reference):

```javascript
// what's needed for the search
import {
	AlgoliaSearch,
	createAlgoliaScript,
} from 'https://cdn.jsdelivr.net/npm/@joblist/algolia-search@0.0.9/index.js'

// what is needed for the map
import {
	MapList,
	companiesResultsToMapMarkers, // serializer, search result -> marker
} from './src/map/index.js'

const init = async () => {
	try {
		await createAlgoliaScript()
		await createLeafletScripts()
	} catch (error) {
		return console.log('Error inserting scripts', error)
	}

	/* listen to algolia search results, to display them on the map;
		alternativelly the map is independant, and any marker would work*/
	document.querySelector('algolia-search').addEventListener('algoliaSearchResult', ({ detail }) => {
		if (detail.indexName === 'prod_companies') {
			const itemMarkers = companiesResultsToMapMarkers(detail.items)
			document.querySelector('map-list').setAttribute(
				'markers',
				JSON.stringify(itemMarkers)
			)
		}
	})

	customElements.define('map-list', MapList)
	customElements.define('algolia-search', AlgoliaSearch)

	console.log('Inited algolia scripts, algolia-search)
}
init()
```

# Components

All attributes (of web components) are of type string: `attribute-name="a-string-always"`

## `<algolia-search/>`

Displays a search input, results and pagination for one or more algolia search indices.

- `app-id` = `ABC123456`, the algolia app id
- `api-key` = `ABC123CDE456FGH789XXXXX`, the algolia app key
- `index-names`[1 minimum] = `prod_jobs,prod_companies`, comma
  separated list of algolia indexes to search. The first one is the
  `main` one, and will be the only one showing pagination etc.
- `routing` [optional] = `false`, should algolia use the URL to show current search, so we can "route" (link) to it
- `autofocus` [optional] = `false`, should the search input be autofocused?
- `show-reset` [optional] = `true`, should there be a "reset search" button next to the search input?
- `origin` [optional] = the root url `https://example.org` that will be used to create links from search results

Emits an event `algoliaSearchResult` each time there are new search results displayed

```javascript
document.querySelector('algolia-search').addEventListener('algoliaSearchResult', (event) => {})

// `event.detail` content
{
	indexName, // the name of the index searched for these results
	searchQuery, // the query that was searched for
	items, // the list of result items
}
```

## `<map-list/>`

Display a [leaflet.js](https://leafletjs.com) map, with optional markers.

- `longitude` = `50.000`
- `latitude` = ``-0.9`
- `zoom` = `1`
- `markers` a list of markers as `JSON.stringify(markers)` data
- `origin`, the root url (`https://example.com/markers`) for markers
  links, to witch `/:slug` (of the marker) will be
  appended. Alternativelly, use `{}` for a pattern to be replaced with
  the slug (`https://example.com/markers/?query={}`)

Where markers:
```js
const markers = [
	{
		text: 'foo',
		slug: 'my-foo-slug',
		latidute: 50.110,
		longitude: -0.09,
	},
	{
		text: bar,
		slug: 'my-bar-slug',
		latidute: 60.001,
		longitude: 10.89,
	},
]
```

# Development

There are dependencies to this projets (npm modules), but those are hard linked from a CDN URL, and injected by the javascript as script tags in the host application. Therefore, no need to npm install.

- `npx serve .` inside this folder to have a local environment server

# Env keys

- algolia (use the normal ones given for the frontend)

# Deployment

To deploy a new production version.

## Build for production

There is not build step, no minification, the project is deployed as is.

## Deployment to production

To deploy a new version to npm, create a new git tag.

It should be the same as the tag on `package.json.version`, and this
will run the `publish` to npm job in `./gitlab-ci.yml`.

The `NPM_TOKEN` is defined at the organisation level.

Note that the variable is available only:
- on protected branch `main`
- on protected tag `v*`
